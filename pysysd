#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    Sample Python daemon template for running under systemd.

    @copyright: 2017
    @license: GPLv3
'''
import sys, os
import logging
import signal


# apt install python3-systemd
from systemd.daemon import notify as sdnotify
from systemd.journal import JournalHandler

__version__ = '0.50'
log = logging.getLogger(__name__)
INTERVAL = 10
running = [True]


def on_shutdown_signal(signum, frame):
    ''' Prepare to shut down gracefully on signal. '''
    print()  # clears line after signal for better readability on the console
    log.warn(u'Received signal %s, shutting down…', signum)
    running.clear()


def setup():
    ''' Parse command-line and set up logging. '''
    from argparse import ArgumentParser, RawTextHelpFormatter
    parser = ArgumentParser(description=__doc__,
                            formatter_class=RawTextHelpFormatter)

    parser.add_argument('-v', '--verbose', action='store_const', dest='loglvl',
                        default=logging.INFO, const=logging.DEBUG)
    parser.add_argument('--version', action='version',
                        version='%(prog)s ' + __version__)

    # parse and validate
    args = parser.parse_args()

    # start logging
    fmt = '%(levelname)-8.8s %(message)s'
    if os.getenv('NOTIFY_SOCKET'):      # detect systemd
        log.setLevel(args.loglvl)
        jh = JournalHandler()
        jh.setFormatter(logging.Formatter(fmt))
        logging.root.addHandler(jh)
    else:
        logging.basicConfig(level=args.loglvl, stream=sys.stdout,
                            format='  ' + fmt)
    return args


def main(args):
    status = os.EX_OK

    try:
        sdnotify('READY=1')
        sdnotify('STATUS=Hunky dory')
        log.info("Top of the mornin' to ya. Version: %s", __version__)

        while running:

            from time import sleep  # to be removed
            log.info('.')
            sleep(10)               # do stuff

        raise RuntimeError('Kablooey!')  # test exception handler

    except Exception as err:
        log.exception('Barf!')
        log.warn('Shutting down now.')
        status = os.EX_SOFTWARE
        #~ os.EX_DATAERR os.EX_NOINPUT os.EX_CANTCREAT os.EX_IOERR os.EX_CONFIG

    finally:
        sdnotify('STOPPING=1')
        log.info('Moriturus te saluto.')

    return status


if __name__ == '__main__':
    signal.signal(signal.SIGINT, on_shutdown_signal)
    signal.signal(signal.SIGTERM, on_shutdown_signal)

    sys.exit(main(setup()))

