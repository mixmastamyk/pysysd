
pysysd
===========

Skeleton of a Python daemon for systemd-based systems.


Installing
--------------

Put the service file under ``/etc/…`` or link it::

    sudo ln -s /full/path/to/pysysd.service /etc/systemd/system/pysysd.service

Edit the service file to point to current location of pysysd::

    [Service]
    # change to the appropriate path below
    ExecStart=/full/path/to/pysysd



Running
-----------

::

    ＞ sudo systemctl status pysysd
    ＞ sudo systemctl start pysysd

    ● pysysd.service - Skeleton of a Python Daemon™
       Loaded: loaded (/full/path/to/pysysd.service; linked; vendor preset: enabled)
       Active: active (running) since Wed 2017-02-22 21:35:25 PST; 1s ago
     Main PID: 9116 (python3)
       Status: "Hunky dory"
        Tasks: 1 (limit: 4915)
       CGroup: /system.slice/pysysd.service
               └─9116 python3 /full/path/to/pysysd

    Feb 22 21:35:25 studioxps systemd[1]: Starting Skeleton of a Python Daemon™...
    Feb 22 21:35:25 studioxps systemd[1]: Started Skeleton of a Python Daemon™.
    Feb 22 21:35:25 studioxps pysysd[9116]: INFO     Top of the mornin' to ya. Version: 0.50
    Feb 22 21:35:25 studioxps pysysd[9116]: INFO     .


Stopped
--------------

I put in a RuntimeError on the way out to illustrate how errors are handled
gracefully::

    ＞ sudo systemctl stop pysysd
    ＞ sudo systemctl status pysysd

    ● pysysd.service - Skeleton of a Python Daemon™
       Loaded: loaded (/full/path/topysysd.service; linked; vendor preset: enabled)
       Active: failed (Result: exit-code) since Wed 2017-02-22 21:36:45 PST; 5s ago
      Process: 9116 ExecStart=pysysd (code=exited, status=70)
     Main PID: 9116 (code=exited, status=70)
       Status: "Hunky dory"

    Feb 22 21:36:35 studioxps pysysd[9116]: INFO     .
    Feb 22 21:36:38 studioxps systemd[1]: Stopping Skeleton of a Python Daemon™...
    Feb 22 21:36:38 studioxps pysysd[9116]: WARNING  Received signal 15, shutting down…
    Feb 22 21:36:45 studioxps pysysd[9116]: ERROR    Barf!
                                             Traceback (most recent call last):
                                               File "pysysd", line 72, in main
                                                 raise RuntimeError('Kablooey!')  # test exception handler
                                             RuntimeError: Kablooey!
    Feb 22 21:36:45 studioxps pysysd[9116]: WARNING  Shutting down now.
    Feb 22 21:36:45 studioxps pysysd[9116]: INFO     Moriturus te saluto.
    Feb 22 21:36:45 studioxps systemd[1]: pysysd.service: Main process exited, code=exited, status=70/n/a
    Feb 22 21:36:45 studioxps systemd[1]: Stopped Skeleton of a Python Daemon™.


Automatic Boot
------------------

::

    ＞ sudo systemctl enable pysysd


Remove
-----------

::

    ＞ sudo systemctl disable pysysd
